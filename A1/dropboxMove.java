package assignment1;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

import org.apache.commons.codec.binary.Base64;
import com.temboo.Library.Dropbox.FileOperations.CopyFileOrFolder;
import com.temboo.Library.Dropbox.FileOperations.CopyFileOrFolder.CopyFileOrFolderInputSet;
import com.temboo.Library.Dropbox.FileOperations.CopyFileOrFolder.CopyFileOrFolderResultSet;
import com.temboo.Library.Dropbox.FileOperations.DeleteFileOrFolder;
import com.temboo.Library.Dropbox.FileOperations.DeleteFileOrFolder.DeleteFileOrFolderInputSet;
import com.temboo.Library.Dropbox.FileOperations.DeleteFileOrFolder.DeleteFileOrFolderResultSet;
import com.temboo.Library.Dropbox.FileOperations.MoveFileOrFolder;
import com.temboo.Library.Dropbox.FileOperations.MoveFileOrFolder.MoveFileOrFolderResultSet;
import com.temboo.Library.Dropbox.FilesAndMetadata.GetFile;
import com.temboo.Library.Dropbox.FilesAndMetadata.GetFile.GetFileInputSet;
import com.temboo.Library.Dropbox.FilesAndMetadata.GetFile.GetFileResultSet;
import com.temboo.Library.Dropbox.OAuth.FinalizeOAuth;
import com.temboo.Library.Dropbox.OAuth.FinalizeOAuth.FinalizeOAuthInputSet;
import com.temboo.Library.Dropbox.OAuth.FinalizeOAuth.FinalizeOAuthResultSet;
import com.temboo.Library.Dropbox.OAuth.InitializeOAuth;
import com.temboo.Library.Dropbox.OAuth.InitializeOAuth.InitializeOAuthInputSet;
import com.temboo.Library.Dropbox.OAuth.InitializeOAuth.InitializeOAuthResultSet;
import com.temboo.core.TembooException;
import com.temboo.core.TembooSession;

public class dropboxMove {
	public static void main(String[] args) throws TembooException, IOException {
		String acctName, appKeyName, appKeyValue;//temboo information
		String dropBoxAppKey, dropBoxAppSecret, dropBoxClientSecret, dropBoxToken;//dropbox information
		int indexVal; 
		String toPath, fromPath, filename;
		Scanner in = new Scanner(System.in);
		//temboo creds
		System.out.println("Please enter the temboo account name: ");
		acctName = in.nextLine();
		
		System.out.println("Please enter the temboo app key name: ");
		appKeyName = in.nextLine();
		
		System.out.println("Please enter the temboo app key value: ");
		appKeyValue = in.nextLine();
		
		System.out.println("Please enter the dropbox app key value: ");
		dropBoxAppKey = in.nextLine();
		
		System.out.println("Please enter the dropbox app secret: ");
		dropBoxAppSecret = in.nextLine();
		
		TembooSession session = new TembooSession(acctName, appKeyName, appKeyValue);//set up the session
		
		InitializeOAuth initializeOAuthChoreo = new InitializeOAuth(session);

		// Get an InputSet object for the choreo
		InitializeOAuthInputSet initializeOAuthInputs = initializeOAuthChoreo.newInputSet();

		// Set inputs
		initializeOAuthInputs.set_DropboxAppSecret(dropBoxAppSecret);
		initializeOAuthInputs.set_DropboxAppKey(dropBoxAppKey);

		// Execute Choreo
		InitializeOAuthResultSet initializeOAuthResults = initializeOAuthChoreo.execute(initializeOAuthInputs);
		
		String url = initializeOAuthResults.get_AuthorizationURL();
		
		System.out.println("Go to this URL to allow access: " + url);//displays the access url
		
		FinalizeOAuth finalizeOAuthChoreo = new FinalizeOAuth(session);

		// Get an InputSet object for the choreo
		FinalizeOAuthInputSet finalizeOAuthInputs = finalizeOAuthChoreo.newInputSet();
		
		// Set inputs
		finalizeOAuthInputs.set_CallbackID(initializeOAuthResults.get_CallbackID());//sets up information for the choreo
		finalizeOAuthInputs.set_DropboxAppSecret(dropBoxAppSecret);
		finalizeOAuthInputs.set_OAuthTokenSecret(initializeOAuthResults.get_OAuthTokenSecret());
		finalizeOAuthInputs.set_DropboxAppKey(dropBoxAppKey);
		
		// Execute Choreo
		FinalizeOAuthResultSet finalizeOAuthResults = finalizeOAuthChoreo.execute(finalizeOAuthInputs);
		System.out.println(finalizeOAuthResults.get_ErrorMessage());
		GetFile getFileChoreo = new GetFile(session);

		// Get an InputSet object for the choreo
		GetFileInputSet getFileInputs = getFileChoreo.newInputSet();
		
		getFileInputs.set_AppSecret(dropBoxAppSecret);//sets up information for the choreo
		getFileInputs.set_AccessToken(finalizeOAuthResults.get_AccessToken());
		getFileInputs.set_AccessTokenSecret(finalizeOAuthResults.get_AccessTokenSecret());
		getFileInputs.set_AppKey(dropBoxAppKey);
		getFileInputs.set_Path("/move/__list");
		
		GetFileResultSet getFileResults = getFileChoreo.execute(getFileInputs);
		
		DeleteFileOrFolder deleteFileOrFolderChoreo = new DeleteFileOrFolder(session);

		// Get an InputSet object for the choreo
		DeleteFileOrFolderInputSet deleteFileOrFolderInputs = deleteFileOrFolderChoreo.newInputSet();

		// Set inputs
		deleteFileOrFolderInputs.set_AccessToken(finalizeOAuthResults.get_AccessToken());//sets up information for the choreo
		deleteFileOrFolderInputs.set_AppSecret(dropBoxAppSecret);
		deleteFileOrFolderInputs.set_AccessTokenSecret(finalizeOAuthResults.get_AccessTokenSecret());
		deleteFileOrFolderInputs.set_AppKey(dropBoxAppKey);
		deleteFileOrFolderInputs.set_Path("/move/__list");

		// Execute Choreo
		DeleteFileOrFolderResultSet deleteFileOrFolderResults = deleteFileOrFolderChoreo.execute(deleteFileOrFolderInputs);
		
		String file = new String(Base64.decodeBase64(getFileResults.get_Response()));		
		
		String[] lines = file.split(System.getProperty("line.separator"));
		
		CopyFileOrFolder copyFileOrFolderChoreo = new CopyFileOrFolder(session);
		CopyFileOrFolderInputSet copyFileOrFolderInputs = copyFileOrFolderChoreo.newInputSet();
		
		
		CopyFileOrFolderResultSet copyFileOrFolderResults = null;
		for(int i =0; i < lines.length; i++)
		{
			indexVal = lines[i].indexOf(" ");//parse out file information
			filename = lines[i].substring(0,indexVal);
			toPath = lines[i].substring(indexVal+1,lines[i].length());
			
			copyFileOrFolderInputs.set_AppSecret(dropBoxAppSecret);//sets up information for the choreo
			copyFileOrFolderInputs.set_AccessToken(finalizeOAuthResults.get_AccessToken());
			copyFileOrFolderInputs.set_AccessTokenSecret(finalizeOAuthResults.get_AccessTokenSecret());
			copyFileOrFolderInputs.set_AppKey(dropBoxAppKey);
			copyFileOrFolderInputs.set_ToPath(toPath+filename);
			copyFileOrFolderInputs.set_FromPath("/move/"+filename);
			
			copyFileOrFolderResults = copyFileOrFolderChoreo.execute(copyFileOrFolderInputs);
			System.out.println("Files moved successfully.");
		}
		
	}
	
}
