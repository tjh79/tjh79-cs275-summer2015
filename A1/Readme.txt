README 

TJ Heiney
Drop Box mover assignment

Temboo choeros being used:
-getFiles
-InitializeOAuth
-FinalizeOAuth
-deleteFileorFolder
-copyFileorFolder

The process
1) You initialize Oauth
2) Visit the link
3) The Oauth is finalized
4) Get the __list from the move folder
5) Store the information in an array
6) delete the __list 
7) Loop over the array making the move call on each element while it is parsed

Problems: I have some trouble walking over the OAUTH process but it seems to be mostly trial and error.

Thoughts on the assignment? I thought it was pretty interesting. Temboo is a very powerful tool. 
