package com.example.thoma_000.myapplication;

import android.os.Bundle;
import android.app.Activity;
import java.io.InputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.HttpURLConnection;
import java.sql.DatabaseMetaData;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.ExecutionException;

import android.os.AsyncTask;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.*;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import static android.app.PendingIntent.getActivity;

public class MainActivity extends Activity{
    ArrayList<String> newData = new ArrayList<String>();
    private DBmanager myDBManager;
    private DBmanager myDBManager2;
    public void setNewData(ArrayList<String> params)
    {
        newData = params;
    }
    public ArrayList<String> getNewData()
    {
        return newData;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button goButton = (Button)this.findViewById(R.id.forecast_button);
        ArrayList<String> newData = null;//new array used by gets and sets to help scope
        goButton.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                ArrayList<String[]> data = new ArrayList<String[]>();//data storage to catch requrn values
                String returnJson = "";//stores a string

                Calendar cal = Calendar.getInstance();//current time
                Calendar calPlusHour = Calendar.getInstance();//current time + 1 hour
                Calendar calConvertedTime = Calendar.getInstance();//converted time from DB
                calPlusHour.add(Calendar.HOUR, 1);
                SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss");
                String date = sdf.format(cal.getTime()).toString();
                System.out.println( sdf.format(calPlusHour.getTime()) );
                myDBManager2 = new DBmanager(MainActivity.this);
                myDBManager2.openReadable();
                String tableContent = myDBManager2.retrieveRows();
                myDBManager2.close();
                if(!tableContent.equals("")) {
                    String newDateString = tableContent.substring(0, tableContent.indexOf(","));//parse out date
                    try {
                        calConvertedTime.setTime(sdf.parse(newDateString));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    if(!tableContent.substring(0,tableContent.indexOf(",")).equals("") && calConvertedTime.get(Calendar.HOUR_OF_DAY) <=  calPlusHour.get(Calendar.HOUR_OF_DAY) && calConvertedTime.get(Calendar.HOUR_OF_DAY) >=  cal.get(Calendar.HOUR_OF_DAY))
                    {
                        ArrayList<String> tempData = new ArrayList<String>();
                        String jsonString = tableContent.substring(tableContent.indexOf(", ")+2, tableContent.length());//parse out json string to be turned into an array
                        JsonArray newJArray = new JsonArray();
                        JsonParser parser = new JsonParser();
                        JsonElement converter = parser.parse(jsonString);
                        newJArray = converter.getAsJsonArray();
                        data = parseToArrayList(newJArray);
                        for(int i = 0; i < data.size(); i++)//convert data to a string array list
                        {
                            String tempString = "";
                            for(int j = 0; j < data.get(i).length; j++) {
                                if (j == 0) {
                                    tempString += data.get(i)[j];
                                } else {
                                    tempString += " " + data.get(i)[j];
                                }

                            }
                            tempData.add(tempString);
                        }

                        setNewData(tempData);
                        ListView lv = (ListView) findViewById(R.id.listView);
                        ArrayAdapter<String> adapter;
                        adapter = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_list_item_1, android.R.id.text1, getNewData());//set adapter
                        lv.setAdapter(adapter);
                    }
                    else
                    {
                        ArrayList<String> tempData = new ArrayList<String>();
                        try {
                            returnJson = new AccessAsync().execute("http://api.wunderground.com/api/c47fc074af5d3159/geolookup/q/autoip.json").get();//calls the async task to retrieve data

                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        } catch (ExecutionException e) {
                            e.printStackTrace();
                        }


                        myDBManager = new DBmanager(MainActivity.this);//sets up db to retrieve
                        JsonArray newJArray = new JsonArray();
                        JsonParser parser = new JsonParser();
                        JsonElement converter = parser.parse(returnJson);
                        newJArray = converter.getAsJsonArray();
                        data = parseToArrayList(newJArray);

                        myDBManager.addRow(date, returnJson);
                        myDBManager.close();

                        for(int i = 0; i < data.size(); i++)
                        {
                            String tempString = "";
                            for(int j = 0; j < data.get(i).length; j++) {
                                if (j == 0) {
                                    tempString += data.get(i)[j];
                                } else {
                                    tempString += " " + data.get(i)[j];
                                }

                            }
                            tempData.add(tempString);
                        }

                        setNewData(tempData);
                        ListView lv = (ListView) findViewById(R.id.listView);
                        TextView txt1 = (TextView) v.findViewById(android.R.id.text1);
                        ArrayAdapter<String> adapter;
                        adapter = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_list_item_1, android.R.id.text1, getNewData());
                        lv.setAdapter(adapter);
                    }
                }
                else
                {
                    ArrayList<String> tempData = new ArrayList<String>();
                    try {
                        returnJson = new AccessAsync().execute("http://api.wunderground.com/api/c47fc074af5d3159/geolookup/q/autoip.json").get();

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }


                    myDBManager = new DBmanager(MainActivity.this);
                    JsonArray newJArray = new JsonArray();
                    JsonParser parser = new JsonParser();
                    JsonElement converter = parser.parse(returnJson);
                    newJArray = converter.getAsJsonArray();
                    data = parseToArrayList(newJArray);

                    myDBManager.addRow(date, returnJson);
                    myDBManager.close();

                    for(int i = 0; i < data.size(); i++)
                    {
                        String tempString = "";
                        for(int j = 0; j < data.get(i).length; j++) {
                            if (j == 0) {
                                tempString += data.get(i)[j];
                            } else {
                                tempString += " " + data.get(i)[j];
                            }

                        }
                        tempData.add(tempString);
                    }

                    setNewData(tempData);
                    ListView lv = (ListView) findViewById(R.id.listView);
                    TextView txt1 = (TextView) v.findViewById(android.R.id.text1);
                    ArrayAdapter<String> adapter;
                    adapter = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_list_item_1, android.R.id.text1, getNewData());
                    lv.setAdapter(adapter);
                }

            }
        });



    }
    private ArrayList<String[]> parseToArrayList(JsonArray ja)//converts json array to array list of string arrays
    {

        ArrayList<String[]> parsed = new ArrayList<>();
        for(int i = 0; i <  ja.size(); i++) {
            String[] hour = new String[5];
            String imageURL = ja.get(i).getAsJsonObject().get("icon_url").getAsString();
            hour[0] = "Image: "+ imageURL;
            String time = ja.get(i).getAsJsonObject().get("FCTTIME").getAsJsonObject().get("pretty").getAsString();
            hour[1] = "Time: "+ time;
            String temp = ja.get(i).getAsJsonObject().get("temp").getAsJsonObject().get("english").getAsString();
            hour[2] = "Temp: "+ temp;
            String conditions = ja.get(i).getAsJsonObject().get("condition").getAsString();
            hour[3] = "Conditions: " + conditions;
            String humidity = ja.get(i).getAsJsonObject().get("humidity").getAsString();
            hour[4] = "Humidity: " + humidity;
            parsed.add(hour);
        }
        return parsed;
    }
    private String openHttpConnection(String urlString) throws IOException{//opens the http connection for the two api calls
        String temp = "";

        InputStream inStream = null;
        InputStream inStream2 = null;
        int checkConn = -1;
        int checkConn2 = -1;
        URL url = new URL(urlString);
        URLConnection conn = url.openConnection();


            HttpURLConnection httpConn = (HttpURLConnection) conn;
            httpConn.setAllowUserInteraction(false);
            httpConn.setInstanceFollowRedirects(true);
            httpConn.setRequestMethod("GET");
            httpConn.connect();
            checkConn = httpConn.getResponseCode();
            if (checkConn == HttpURLConnection.HTTP_OK)
            {
                inStream = httpConn.getInputStream();

                JsonParser jp = new JsonParser();
                JsonElement root = jp.parse(new InputStreamReader((InputStream) inStream));
                JsonObject rootobj = root.getAsJsonObject();

                int userZipcode = rootobj.get("location").getAsJsonObject().get("zip").getAsInt();
                String userCity = rootobj.get("location").getAsJsonObject().get("city").getAsString();
                String userState = rootobj.get("location").getAsJsonObject().get("state").getAsString();
                double userLat = rootobj.get("location").getAsJsonObject().get("lat").getAsDouble();
                double userLon = rootobj.get("location").getAsJsonObject().get("lon").getAsDouble();

                String wuURL2 = "http://api.wunderground.com/api/c47fc074af5d3159/hourly/q/"+userState+"/"+userCity+".json";
                URL url2 = new  URL(wuURL2);
                URLConnection conn2 = url2.openConnection();

                HttpURLConnection httpConn2 = (HttpURLConnection) conn2;
                httpConn2.setAllowUserInteraction(false);
                httpConn2.setInstanceFollowRedirects(true);
                httpConn2.setRequestMethod("GET");
                httpConn2.connect();
                checkConn2 = httpConn2.getResponseCode();

                inStream2 = httpConn2.getInputStream();

                JsonParser jp2 = new JsonParser();
                JsonElement root2 = jp.parse(new InputStreamReader((InputStream) inStream2));
                JsonObject rootobj2 = root2.getAsJsonObject();
                JsonArray  hourlyForecast = rootobj2.get("hourly_forecast").getAsJsonArray();

               temp = hourlyForecast.toString();
            }


        return temp;
    }

    private class AccessAsync extends AsyncTask<String, Void, String> {//sets up async task
        private ArrayList<String[]> returnVal = null;
        @Override
        protected String doInBackground(String... urladds) {
            try {
                return openHttpConnection(urladds[0]);
            } catch (IOException e) {
                e.printStackTrace();

            }
            return null;
        }

    }
}
