package com.example.thoma_000.myapplication;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by thoma_000 on 8/13/2015.
 */
public class DBmanager {
    public static final String DB_NAME = "weather";//name of DB
    public static final String DB_TABLE = "forecast";//name of table
    public static final int DB_VERSION = 7;//working version
    private static final String CREATE_TABLE = "CREATE TABLE " + DB_TABLE + " (dateTime TEXT PRIMARY KEY, dataString TEXT);";//creates the table
    private SQLHelper helper;
    private SQLiteDatabase db;
    private Context context;

    public DBmanager(Context c)//manages the dp
    {
        this.context = c;
        helper = new SQLHelper(c);
        this.db = helper.getWritableDatabase();
    }

    public DBmanager openReadable() throws android.database.SQLException {//opens table to be read
        helper = new SQLHelper(context);
        db = helper.getReadableDatabase();
        return this;
    }

    public void close()//closes table
    {
        helper.close();
    }

    public void addRow(String dateTime, String dataString){//adds rows
        ContentValues newTimeForecast = new ContentValues();
        deleteRow();
        newTimeForecast.put("dateTime", dateTime);
        newTimeForecast.put("dataString", dataString);


        try{
            db.insertOrThrow(DB_TABLE,null,newTimeForecast);
        }
        catch(Exception e)
        {
            System.out.println("Error in inserting");
            e.printStackTrace();
        }
    }

    public void deleteRow()//deletes rows
    {
        db.delete(DB_TABLE, null, null);
    }

    public String retrieveRows()//retrieve rows
    {
        String[] columns = new String[]{"dateTime","dataString"};
        Cursor cursor =  db.query(DB_TABLE,columns, null, null, null, null, null);
        String tablerows = "";
        cursor.moveToFirst();

        while(cursor.isAfterLast() == false)
        {
            tablerows= tablerows + cursor.getString(0) +", " + cursor.getString(1) + "\n";
            cursor.moveToNext();
        }
        if(cursor != null && !cursor.isClosed())
        {
            cursor.close();
        }
        return tablerows;
    }

    public class SQLHelper extends SQLiteOpenHelper//helper class to execture sequel from above
    {
        public SQLHelper(Context c)
        {
            super(c, DB_NAME, null, DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db)
        {
            db.execSQL(CREATE_TABLE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
        {
            System.out.println("Upgrading");
            db.execSQL("DROP TABLE IF EXISTS " + DB_TABLE);
            onCreate(db);
        }
    }

}
