package com.example.thoma_000.tictactoe;

/**
 * Created by thoma_000 on 8/3/2015.
 */
public class Board {
    private String[][] mainBoard = {{"-","-","-"},
                                {"-","-","-"},
                                {"-","-","-"}};//underlying data structure
    private boolean player1Turn = true;
    private boolean player2Turn = false;


    public void setPlayer1(boolean bool)//set player
    {
        player1Turn = bool;
    }
    public void setPlayer2(boolean bool)//set player
    {
        player2Turn = bool;
    }
    public void reset(){
        for(int i = 0; i < 3; i++)
        {
            for(int j = 0; j < 3; j++)
            {
                mainBoard[i][j] = "-";//reset board
            }
        }
    }

    public String[][] getMainBoard()//return board
    {
        return mainBoard;
    }

    public String insert(int x, int y) {//insert pieces based on coords determined in main activity
        String playerPiece = "z";
        if (mainBoard[x][y] == "-" && mainBoard[x][y] != "o" && player1Turn) {
            mainBoard[x][y] = "x";
            playerPiece = "x";
            player1Turn = false;
            player2Turn = true;
        } else if (mainBoard[x][y] == "-" && mainBoard[x][y] != "x" && player2Turn) {
            mainBoard[x][y] = "o";
            playerPiece = "o";
            player1Turn = true;
            player2Turn = false;
        }

        return playerPiece;
    }

    public String winner() {
        String winningPlayer = "";
        if (mainBoard[0][0] == mainBoard[1][1] && mainBoard[1][1] == mainBoard[2][2])//diagonal win
        {
            if(mainBoard[0][0].equals("x") && mainBoard[1][1].equals("x") && mainBoard[2][2].equals("x"))
            {
                winningPlayer = "Player 1";
                return winningPlayer;
            }
            else if(mainBoard[0][0].equals("o") && mainBoard[1][1].equals("o") && mainBoard[2][2].equals("o"))
            {
                winningPlayer = "Player 2";
                return winningPlayer;
            }
        }
        if (mainBoard[0][2] == mainBoard[1][1] && mainBoard[1][1] == mainBoard[2][0])//diagonal win
        {
            if(mainBoard[0][2].equals("x") && mainBoard[1][1].equals("x") && mainBoard[2][0].equals("x"))
            {
                winningPlayer = "Player 1";
                return winningPlayer;
            }
            else if(mainBoard[0][2].equals("o") && mainBoard[1][1].equals("o") && mainBoard[2][0].equals("o"))
            {
                winningPlayer = "Player 2";
                return winningPlayer;
            }
        }

        for(int k = 0; k < 3; k++)
        {
            if(mainBoard[0][k] == mainBoard[1][k] && mainBoard[1][k] == mainBoard[2][k])//cols
            {
                if(mainBoard[0][k].equals("x") && mainBoard[1][k].equals("x") && mainBoard[2][k].equals("x"))
                {
                    winningPlayer = "Player 1";
                    return winningPlayer;
                }
                else if(mainBoard[0][k].equals("o") && mainBoard[1][k].equals("o") && mainBoard[2][k].equals("o"))
                {
                    winningPlayer = "Player 2";
                    return winningPlayer;
                }
            }
            if(mainBoard[k][0] == mainBoard[k][1] && mainBoard[k][1] == mainBoard[k][2])//rows
            {
                if(mainBoard[k][0].equals("x") && mainBoard[k][1].equals("x") && mainBoard[k][2].equals("x"))
                {
                    winningPlayer = "Player 1";
                    return winningPlayer;
                }
                else if(mainBoard[k][0].equals("o") && mainBoard[k][1].equals("o") && mainBoard[k][2].equals("o"))
                {
                    winningPlayer = "Player 2";
                    return winningPlayer;
                }
            }
        }
        return winningPlayer;
    }

}
