package com.example.thoma_000.tictactoe;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {
    private Board board = new Board();
    private int count= 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        board = new Board();
    }

    public void resetClick(View v)
    {
        board.reset();//reset board
        board.setPlayer1(true);//set player1 back to main
        board.setPlayer2(false);//set off player2
        Button place11 = (Button) findViewById(R.id.button11);//reset button
        place11.setText("-");
        place11.setClickable(true);
        Button place12 = (Button) findViewById(R.id.button12);//reset button
        place12.setText("-");
        place12.setClickable(true);
        Button place13 = (Button) findViewById(R.id.button13);//reset button
        place13.setText("-");
        place13.setClickable(true);
        Button place21 = (Button) findViewById(R.id.button21);//reset button
        place21.setText("-");
        place21.setClickable(true);
        Button place22 = (Button) findViewById(R.id.button22);//reset button
        place22.setText("-");
        place22.setClickable(true);
        Button place23 = (Button) findViewById(R.id.button23);//reset button
        place23.setText("-");
        place23.setClickable(true);
        Button place31 = (Button) findViewById(R.id.button31);//reset button
        place31.setText("-");
        place31.setClickable(true);
        Button place32 = (Button) findViewById(R.id.button32);//reset button
        place32.setText("-");
        place32.setClickable(true);
        Button place33 = (Button) findViewById(R.id.button33);//reset button
        place33.setText("-");
        place33.setClickable(true);
        TextView display = (TextView) findViewById(R.id.display);
        display.setText("");
        count = 0;
    }

    public void buttonClick(View v)
    {
        String value = "";//the value being inserted
        String win = "";
        Button place = (Button) findViewById(v.getId());//get the button

        if(place.getId() == R.id.button11)//place the piece
        {
            value = board.insert(0,0);
            place.setText(value);
            place.setClickable(false);
        }
        else if(place.getId() == R.id.button12)//place the piece
        {
            value = board.insert(0,1);
            place.setText(value);
            place.setClickable(false);
        }
        else if(place.getId() == R.id.button13)//place the piece
        {
            value = board.insert(0,2);
            place.setText(value);
            place.setClickable(false);
        }
        else if(place.getId() == R.id.button21)//place the piece
        {
            value = board.insert(1,0);
            place.setText(value);
            place.setClickable(false);
        }
        else if(place.getId() == R.id.button22)//place the piece
        {
            value = board.insert(1,1);
            place.setText(value);
            place.setClickable(false);
        }
        else if(place.getId() == R.id.button23)//place the piece
        {
            value = board.insert(1,2);
            place.setText(value);
            place.setClickable(false);
        }
        else if(place.getId() == R.id.button31)//place the piece
        {
            value = board.insert(2,0);
            place.setText(value);
            place.setClickable(false);
        }
        else if(place.getId() == R.id.button32)//place the piece
        {
            value = board.insert(2,1);
            place.setText(value);
            place.setClickable(false);
        }
        else if(place.getId() == R.id.button33)//place the piece
        {
            value = board.insert(2,2);
            place.setText(value);
            place.setClickable(false);
        }

        win = board.winner();

        count++;
        if(count >= 9)
        {
            TextView display = (TextView) findViewById(R.id.display);
            display.setText("Draw!");
        }
        if(!win.equals(""))
        {
            TextView display = (TextView) findViewById(R.id.display);
            display.setText(win + " wins!");
            Button place11 = (Button) findViewById(R.id.button11);//reset button
            place11.setClickable(false);
            Button place12 = (Button) findViewById(R.id.button12);//reset button
            place12.setClickable(false);
            Button place13 = (Button) findViewById(R.id.button13);//reset button
            place13.setClickable(false);
            Button place21 = (Button) findViewById(R.id.button21);//reset button
            place21.setClickable(false);
            Button place22 = (Button) findViewById(R.id.button22);//reset button
            place22.setClickable(false);
            Button place23 = (Button) findViewById(R.id.button23);//reset button
            place23.setClickable(false);
            Button place31 = (Button) findViewById(R.id.button31);//reset button
            place31.setClickable(false);
            Button place32 = (Button) findViewById(R.id.button32);//reset button
            place32.setClickable(false);
            Button place33 = (Button) findViewById(R.id.button33);//reset button
            place33.setClickable(false);
        }
    }
}
