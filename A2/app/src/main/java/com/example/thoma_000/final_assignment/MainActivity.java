package com.example.thoma_000.final_assignment;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;


import com.temboo.Library.Google.Directions.GetDrivingDirections;
import com.temboo.Library.Google.Directions.GetDrivingDirections.GetDrivingDirectionsInputSet;
import com.temboo.Library.Google.Directions.GetDrivingDirections.GetDrivingDirectionsResultSet;
import com.temboo.Library.Google.OAuth.InitializeOAuth;
import com.temboo.core.TembooException;
import com.temboo.core.TembooSession;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import com.google.gson.*;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import com.temboo.Library.Google.OAuth.FinalizeOAuth;
import com.temboo.Library.Google.OAuth.FinalizeOAuth.FinalizeOAuthInputSet;
import com.temboo.Library.Google.OAuth.FinalizeOAuth.FinalizeOAuthResultSet;
import com.temboo.Library.Google.OAuth.InitializeOAuth;
import com.temboo.Library.Google.OAuth.InitializeOAuth.InitializeOAuthInputSet;
import com.temboo.Library.Google.OAuth.InitializeOAuth.InitializeOAuthResultSet;

public class MainActivity extends AppCompatActivity {
    int counter = 0;
    int counterLimit = 4;
    ArrayList<String[]> addressCol = new ArrayList();//sets address
    ArrayList<String> addressStringCol = new ArrayList();//collects strings
    ArrayList<String> finalDir = new ArrayList();//the final directions being pushed
    ArrayList<String[]> directionsCol = new ArrayList();//a collection of directions
    HashMap<int[], ArrayList<String>> timeToDirections = new HashMap<int[], ArrayList<String>>();//map of information for travelling sales man problem
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button goButton = (Button)this.findViewById(R.id.Enter_button);
        Button directionsButtonMain = (Button)this.findViewById(R.id.direction_button);

        goButton.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {//submit text on click
                if(counter < counterLimit) {
                    String[] tempAddress = new String[3];
                    EditText addressText = (EditText) findViewById(R.id.editText);//get text from text field 1
                    String addressString = addressText.getText().toString();
                    addressText.setText("");

                    EditText addressText2 = (EditText) findViewById(R.id.editText2);//get text from text field 2
                    String addressString2 = addressText2.getText().toString();
                    addressText2.setText("");

                    EditText addressText3 = (EditText) findViewById(R.id.editText3);//get text from text field 3
                    String addressString3 = addressText3.getText().toString();
                    addressText3.setText("");
                    tempAddress[0] = addressString;
                    tempAddress[1] = addressString2;
                    tempAddress[2] = addressString3;

                    addressCol.add(tempAddress);
                    counter += 1;

                    if(counter >= counterLimit)
                    {
                        TextView enteredAddressMessages = (TextView) findViewById(R.id.displayText);
                        enteredAddressMessages.setText("You have entered "+counter+" addresses. Click below to get directions.");

                        Button directionsButtonTemp = (Button) findViewById(R.id.direction_button);
                        directionsButtonTemp.setVisibility(View.VISIBLE);
                    }
                }


            }
        });

        directionsButtonMain.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {//get directions on click

                ArrayList<String> returnedArray = new ArrayList<String>();
                try {
                    returnedArray = new AccessAsync().execute().get();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }

                ListView lv = (ListView) findViewById(R.id.listView);
                TextView txt1 = (TextView) v.findViewById(android.R.id.text1);
                ArrayAdapter<String> adapter;
                adapter = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_list_item_1, android.R.id.text1, returnedArray);//display array in list view
                lv.setAdapter(adapter);
            }
        });
    }

    private ArrayList<String> parseJsonArray(String arrayAsString)//parse a json array to get the individual directions
    {
        ArrayList<String> directionsTemp = new ArrayList();
        JsonArray newJArray = new JsonArray();
        JsonParser parser = new JsonParser();
        JsonElement converter = parser.parse(arrayAsString);
        newJArray = converter.getAsJsonArray();
        for(int i = 0; i <  newJArray.size(); i++) {
            directionsTemp.add(newJArray.get(i).getAsJsonObject().get("html_instructions").getAsString());
        }

        return directionsTemp;
    }

    private ArrayList<String> openHttpConnection() throws IOException, TembooException {//opens the http connection for the two api calls
        ArrayList<String> temp = new ArrayList<>();
        TembooSession session = new TembooSession("tjh79", "myFirstApp", "af07ff17f4e84a5ead5ed6e7499eaa95");//sets up temboo

        String addressNewString = "";

        for(int i =0; i < addressCol.size(); i++)//formats addresses in forms temboo can read
        {
            for(int j = 0; j < addressCol.get(i).length; j++)
            {
                if(j == 0)
                {
                    addressNewString = "" + addressCol.get(i)[j];
                }
                else {
                    addressNewString += "," + addressCol.get(i)[j];
                }
            }
            addressStringCol.add(addressNewString);

        }
        int newCount = 0;

        while(newCount < counterLimit) {

            for (int k = newCount; k < addressStringCol.size() - 1; k++) {
                String[] dir = new String[5];
                GetDrivingDirections getDrivingDirectionsChoreo = new GetDrivingDirections(session);
                GetDrivingDirectionsInputSet getDrivingDirectionsInputs = getDrivingDirectionsChoreo.newInputSet();
                getDrivingDirectionsInputs.set_Origin(addressStringCol.get(newCount));
                getDrivingDirectionsInputs.set_Destination(addressStringCol.get(k+1));
                GetDrivingDirectionsResultSet getDrivingDirectionsResults = getDrivingDirectionsChoreo.execute(getDrivingDirectionsInputs);//call for the directions
                JsonParser jp = new JsonParser();
                JsonElement root = jp.parse(getDrivingDirectionsResults.get_Response());
                JsonObject rootobj = root.getAsJsonObject();
                JsonArray routes = rootobj.get("routes").getAsJsonArray();
                String duration = routes.get(0).getAsJsonObject().get("legs").getAsJsonArray().get(0).getAsJsonObject().get("duration").getAsJsonObject().get("text").getAsString();
                JsonArray directionsArray = routes.get(0).getAsJsonObject().get("legs").getAsJsonArray().get(0).getAsJsonObject().get("steps").getAsJsonArray();
                String directionsAsString = directionsArray.toString();
                dir[0] = duration;
                dir[1] = addressStringCol.get(newCount);
                dir[2] = addressStringCol.get(k+1);
                dir[3] = directionsAsString;
                dir[4] = ""+newCount;
                directionsCol.add(dir);//add what we returned to collection
            }
            newCount++;
        }
        for(int m = 0; m < directionsCol.size(); m ++)
        {
                int[] duple = new int[2];
               ArrayList<String> tempHolder = new ArrayList<String>();//a temp array list
               int haveHours = directionsCol.get(m)[0].indexOf("hours");
            if(haveHours < 0) {//converts from string to int for determinig time.
                int haveHours2 = directionsCol.get(m)[0].indexOf("hour");
                if (haveHours2 > 0) {
                    String hour = directionsCol.get(m)[0].substring(0, directionsCol.get(m)[0].indexOf("hour"));
                    int hoursAsMins = Integer.parseInt(hour.substring(0, 1)) * 60;
                    int minutes = Integer.parseInt(directionsCol.get(m)[0].substring(directionsCol.get(m)[0].indexOf("hour") + 5, directionsCol.get(m)[0].indexOf("min") - 1));
                    int total_time = hoursAsMins + minutes;
                    tempHolder = parseJsonArray(directionsCol.get(m)[3]);
                    duple[0] = Integer.parseInt(directionsCol.get(m)[4]);
                    duple[1] = total_time;
                    timeToDirections.put(duple, tempHolder);
                }
                else
                {
                    int minutes = Integer.parseInt(directionsCol.get(m)[0].substring(0, directionsCol.get(m)[0].indexOf("min") - 1));
                    int total_time = minutes;
                    tempHolder = parseJsonArray(directionsCol.get(m)[3]);
                    duple[0] = Integer.parseInt(directionsCol.get(m)[4]);
                    duple[1] = total_time;
                    timeToDirections.put(duple, tempHolder);

                }
            }
            else
            {
                String hours = directionsCol.get(m)[0].substring(0, directionsCol.get(m)[0].indexOf("hours"));
                int hoursAsMins = Integer.parseInt(hours.substring(0, 1)) * 60;
                int minutes = Integer.parseInt(directionsCol.get(m)[0].substring(directionsCol.get(m)[0].indexOf("hours") + 6, directionsCol.get(m)[0].indexOf("min") - 1));
                int total_time = hoursAsMins + minutes;
                tempHolder = parseJsonArray(directionsCol.get(m)[3]);
                duple[0] = Integer.parseInt(directionsCol.get(m)[4]);
                duple[1] = total_time;
                timeToDirections.put(duple, tempHolder);
            }
        }

        int min = -1;
        for(Map.Entry<int[], ArrayList<String>> entry : timeToDirections.entrySet())
        {
                for(int a = 0; a < entry.getValue().size(); a++)
                        {
                            finalDir.add("entry #" +entry.getKey()[0] +" : " + entry.getValue().get(a));//add to the map based on the time value as key

                        }
        }

        temp = finalDir;
        return temp;
    }

    private class AccessAsync extends AsyncTask<Void, Void, ArrayList<String>> {//sets up async task
        private ArrayList<String[]> returnVal = null;
        @Override
        protected ArrayList<String> doInBackground(Void... params) {
            try {
                return openHttpConnection();
            } catch (IOException e) {
                e.printStackTrace();

            } catch (TembooException e) {
                e.printStackTrace();
            }
            return null;
        }

    }

}
