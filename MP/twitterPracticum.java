package midterm;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;

import com.temboo.Library.Twitter.OAuth.FinalizeOAuth;
import com.temboo.Library.Twitter.OAuth.FinalizeOAuth.FinalizeOAuthInputSet;
import com.temboo.Library.Twitter.OAuth.FinalizeOAuth.FinalizeOAuthResultSet;
import com.temboo.Library.Twitter.OAuth.InitializeOAuth;
import com.temboo.Library.Twitter.OAuth.InitializeOAuth.InitializeOAuthInputSet;
import com.temboo.Library.Twitter.OAuth.InitializeOAuth.InitializeOAuthResultSet;
import com.temboo.Library.Twitter.Timelines.UserTimeline;
import com.temboo.Library.Twitter.Timelines.UserTimeline.UserTimelineInputSet;
import com.temboo.Library.Twitter.Timelines.UserTimeline.UserTimelineResultSet;
import com.temboo.Library.Twitter.Users.Lookup;
import com.temboo.Library.Twitter.Users.Lookup.LookupInputSet;
import com.temboo.Library.Twitter.Users.Lookup.LookupResultSet;
import com.temboo.core.TembooException;
import com.temboo.core.TembooSession;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.net.HttpURLConnection;
import java.net.URL;

public class twitterPracticum {
	public static void main(String[] args) throws TembooException, IOException {
		String acctName, appKeyName, appKeyValue;//temboo information
		String twitterAppKey, twitterAppSecret;//dropbox information
		int indexVal; //value of the index
		int numTweets = 30, grade; //max number of tweets
		String screenName, tweet, singleWord;//twitter information
		String[] individualWords;//stores the individual word
		ArrayList<String[]> tweetWordList = new ArrayList();//stores the broken down tweet
		String wordnikURL = "";//creates a url
	    URL wnURL = null; //sets url
	    HttpURLConnection request = null;//creates connection
		int totalSylCount = 0;//syllable count
		String totalWordsIgnored = "";//what words were ignored
		
		double firstCon = 1.0430, secondCon = 30, thirdCon = 3.1291;//declaring constants
		
	    
		
		Scanner in = new Scanner(System.in);
		//temboo creds
		System.out.println("Please enter the temboo account name: ");
		acctName = in.nextLine();
		
		System.out.println("Please enter the temboo app key name: ");
		appKeyName = in.nextLine();
		
		System.out.println("Please enter the temboo key value: ");
		appKeyValue = in.nextLine();
		
		System.out.println("Please enter the twitter app key: ");
		twitterAppKey = in.nextLine();
		
		System.out.println("Please enter the twitter app secret: ");
		twitterAppSecret = in.nextLine();
		
		TembooSession session = new TembooSession(acctName, appKeyName, appKeyValue);//set up the session
		
		InitializeOAuth initializeOAuthChoreo = new InitializeOAuth(session);

		// Get an InputSet object for the choreo
		InitializeOAuthInputSet initializeOAuthInputs = initializeOAuthChoreo.newInputSet();

		// Set inputs
		initializeOAuthInputs.set_ConsumerSecret(twitterAppSecret);
		initializeOAuthInputs.set_ConsumerKey(twitterAppKey);

		// Execute Choreo
		InitializeOAuthResultSet initializeOAuthResults = initializeOAuthChoreo.execute(initializeOAuthInputs);
		
		String url = initializeOAuthResults.get_AuthorizationURL();
		
		System.out.println("Go to this URL to allow access: " + url);//displays the access url
		
		FinalizeOAuth finalizeOAuthChoreo = new FinalizeOAuth(session);

		// Get an InputSet object for the choreo
		FinalizeOAuthInputSet finalizeOAuthInputs = finalizeOAuthChoreo.newInputSet();

		// Set inputs
		finalizeOAuthInputs.set_CallbackID(initializeOAuthResults.get_CallbackID());
		finalizeOAuthInputs.set_OAuthTokenSecret("ZXz");
		finalizeOAuthInputs.set_ConsumerSecret(twitterAppSecret);
		finalizeOAuthInputs.set_ConsumerKey(twitterAppKey);

		// Execute Choreo
		FinalizeOAuthResultSet finalizeOAuthResults = finalizeOAuthChoreo.execute(finalizeOAuthInputs);
		
		System.out.println("Enter the screen name. This is the @###### but please leave off the @: ");//prompt the user for the userID
		screenName = in.nextLine();
		
		UserTimeline userTimelineChoreo = new UserTimeline(session);

		// Get an InputSet object for the choreo
		UserTimelineInputSet userTimelineInputs = userTimelineChoreo.newInputSet();

		// Set inputs
		userTimelineInputs.set_ScreenName(screenName);
		userTimelineInputs.set_AccessToken(finalizeOAuthResults.get_AccessToken());
		userTimelineInputs.set_AccessTokenSecret(finalizeOAuthResults.get_AccessTokenSecret());
		userTimelineInputs.set_ConsumerSecret(twitterAppSecret);
		userTimelineInputs.set_ConsumerKey(twitterAppKey);
		userTimelineInputs.set_Count(numTweets);

		// Execute Choreo
		UserTimelineResultSet userTimelineResults = userTimelineChoreo.execute(userTimelineInputs);
		
		JsonParser jp = new JsonParser();//first parser for tweets 
    	JsonElement root = jp.parse(userTimelineResults.get_Response());
    	JsonArray tweetArray = root.getAsJsonArray();
      
    	for(int i = 0; i < tweetArray.size(); i++)
    	{
    		tweet = tweetArray.get(i).getAsJsonObject().get("text").getAsString();
    		individualWords = tweet.split(" ");
    		tweetWordList.add(individualWords);
    	}
    	
    	JsonParser jp2 = new JsonParser();//parser for wordnik 
    	JsonElement root2 = null;
    	JsonArray countReturn = null;
    	
    	for(int j = 0; j < tweetWordList.size(); j++)//loop through the array list
    	{
    		for(int k = 0; k < tweetWordList.get(j).length; k++)//loop through the array
    		{
    			singleWord = tweetWordList.get(j)[k].replaceAll("[^A-Za-z0-9 ]", "");//clear out non-letters
    			singleWord = singleWord.replaceAll(" ", "");//clear out spaces
    			
    			if(singleWord != null && !singleWord.equals("") && !singleWord.equals(" "))
    			{
    				wordnikURL = "http://api.wordnik.com/v4/word.json/"+singleWord+"/hyphenation?useCanonical=false&limit=50&api_key=a2a73e7b926c924fad7001ca3111acd55af2ffabf50eb4ae5";//create the api call
    				wnURL = new  URL(wordnikURL);
    		    	request = (HttpURLConnection) wnURL.openConnection(); 
    		    	request.connect();//make the call
    		    	
    		    	
    		    	root2 = jp2.parse(new InputStreamReader((InputStream) request.getContent()));
    		    	countReturn = root2.getAsJsonArray();//find size of the array also know as syllable count
    		    	
    		    	if(countReturn.size() >= 3)
    		    	{
    		    		totalSylCount += 1;
    		    	}
    		    	else if(countReturn.size() <= 0)
    		    	{
    		    		totalWordsIgnored += " " + singleWord + "\n";
    		    	
    		    	}
    			}
    		}
    	}
    	
    	System.out.println("Total Count of polysyllable words: " + totalSylCount);
    	//System.out.println("Words ignored by WordNik: "+ totalWordsIgnored); 
    	
    	grade = (int) (firstCon * Math.sqrt(totalSylCount * (secondCon/numTweets)) + thirdCon);//equation
    	
    	System.out.println("Grade Level: " + grade);
    	
	}
}
